<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/register', function (){
    return redirect('/login');
});

Route::get('/', function (){
    return redirect('/login');
});

Route::group(['middleware' => ['auth']], function () {

    Route::get('/', function (){
        return redirect('/task');
    });

    Route::get('home', function (){
        return redirect('/task');
    });

    Route::resource('task','TaskController');

    Route::resource('comment','CommentController');

    Route::resource('media','MediaController');

    Route::delete('mediaDeleteByTask','MediaController@mediaDeleteByTask');

    Route::post('change-status', 'TaskController@changeStatus');

    Route::post('changeTodoStatus', 'TaskController@changeTodoStatus');

//    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('getServiceTeams/{id}', 'ServiceController@getServiceTeams');
    Route::get('show-team-serviecs', 'ServiceController@showTeamAndServices');
    Route::get('deleteTeamServices/{id}', 'ServiceController@deleteTeamServices');
    Route::get('deleteServices/{id}', 'ServiceController@destroy');

    Route::post('storeTeamsServices', 'ServiceController@storeTeamsServices')->name('storeTeamsServices');
    //------------------------------service
    Route::get('addservices','ServiceController@index');
    Route::resource('service',"ServiceController");
//    Route::post('service/{id}','ServiceController@destroy');

//------------------Organization------------------------
    Route::resource('organization','OrganizationController');

    Route::put('organizations/{id}','OrganizationController@update');

    Route::post('delorganization/{id}','OrganizationController@destroy');

    Route::get('getorganization/{id}','OrganizationController@show');
//-----------------------------------------------------------

//------------------Department
    Route::post('department','DepartmentController@store')->name('department');

    Route::post('department/{id}','DepartmentController@destroy');

    Route::get('getdepartment/{id}','DepartmentController@show');

    Route::put('updatedepartment/{id}','DepartmentController@update');

//-------------------------------------------------------------

//------------------Team--------------
    Route::resource('team','TeamController');

    Route::post('delteam/{id}','TeamController@destroy');

    Route::get('getAgentDetail/{id}','TeamController@show');

    Route::put('updateteam/{id}','TeamController@update');

//------------------teamMember
    Route::resource('teamMember','TeamMemberController');

    Route::post('delteammember/{id}','TeamMemberController@destroy');

    Route::get('teamMember/{id}','TeamMemberController@show');

    Route::put('teamMembers/{id}','TeamMemberController@update');

    Route::get('getodep/{id}','DepartmentController@getodep');

    Route::get('getTeamMember/{id}','TeamMemberController@index')->name('getTeamMember');
//-------------------------------------------------------------


    Route::resource('users', 'UserController')->middleware('isAdmin');

    Route::resource('roles', 'RoleController')->middleware('isAdmin');

    Route::resource('permissions', 'PermissionController')->middleware('isAdmin');

//------------------Task Manager
    Route::get('services', function () {
        return view('admin.services.index');
    });

    Route::get('account-manager', function () {
        return view('admin.task-management.account-manager-task-detail');
    });

    Route::resource('type','TypeController');

});
