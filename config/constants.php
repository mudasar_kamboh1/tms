<?php

    return [
        'STATUSES' => [
            'pending' => '#FF00FF',
            'working' => '#008000',
            'completed' => '#00FF00',
            'rejected' => '#FF0000'
        ]
    ];