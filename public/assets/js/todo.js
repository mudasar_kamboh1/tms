function addTodoItem() {
  var todoItem = $("#new-todo-item").val();

  if(todoItem != ''){
      $("#todo-list").append("<li><label class='fancy-label'><input type='checkbox'" +
          " name='todo-item-done'" +
          " class='todo-item-done'"+
          " />" +
          "<span class='label-text'></span></label>" +
          "<input type='text'" +
          " name='toDoTitle[]' class='select-item-done lineInput' value='" + todoItem + "'  />" +
          "<input type='hidden'" +
          " name='toDo[]'" +
          "class='todo-item-done'" +
          "/>" +
          "</li>");
  }
 $("#new-todo-item").val("");
}

function deleteTodoItem(e, item) {
  e.preventDefault();
  $(item).parent().fadeOut('slow', function() { 
    $(item).parent().remove();
  });
}

                           
function completeTodoItem() {
  $(this).closest('li').find(' input[name="toDoTitle[]"]').toggleClass("strike");
 if($(this).closest('li').find(' input[name="toDoTitle[]"]').hasClass("strike")){
   $(this).closest('li').find(' input[name="toDo[]"]').val(1);
 }else{
   $(this).closest('li').find(' input[name="toDo[]"]').val('');
 }
 
}


$(function() {
 
   $("#add-todo-item").on('click', function(e){
     e.preventDefault();
     addTodoItem()
   });
  
//EVENT DELEGATION
//#todo-list is the event handler because .todo-item-delete doesn't exist when the document loads, it is generated later by a todo entry
//https://learn.jquery.com/events/event-delegation/
  $("#todo-list").on('click', '.todo-item-delete', function(e){
    var item = this; 
    deleteTodoItem(e, item)
  })
  
  $(document).on('click', ".todo-item-done", completeTodoItem)

});