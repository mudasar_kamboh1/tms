<?php

namespace App\Http\Controllers;

use App\Organization;
use App\Team;
use App\TeamMember;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class TeamMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id){
        $user = Auth::user();
        if($user->hasRole('Admin')){
            //Getting Specific Team
            $agents = Team::find($id);
            $teams = Team::whereId($id)->first();
        }else{
            //Getting Logged in user team
            $agents = $user->teamMember->team;
            $teams = Team::whereId($user->teamMember->team->id)->first();
        }


        if(is_null($agents)){
            toastr()->error('Team not exist!');
            return back();
        }
        $agents = $agents->teamMembers;

        $roles = Role::where('name','!=', 'Admin')->get();

        return view('admin.teamMember.list',compact('agents','teams','roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $auth = Auth::user();
//
//        if(!$auth->hasRole('Add Team Member')){
//            toastr()->warning('No permission!');
//            return back();
//        }

        $alreadyExist = User::where('email',$request['email'])->first();
        if(!is_null($alreadyExist)){
            toastr()->error('Email already exist!');
            return back();
        }

        $userInput = $request->except('team_id');
//        $password = str_random(6);
//        $userInput['password'] = bcrypt($password);
        $user = User::create($userInput);

        $team_member = TeamMember::create([
            'team_id' => $request['team_id'],
        ]);

//        'account_manager' => $request->keys('account_manager') ? $request->account_manager : 0
        $team_member->user()->associate($user)->save();

        $roles = $request['roles']; //Retrieving the roles field
        //Checking if a role was selected
        if (isset($roles)) {

            foreach ($roles as $role) {
                $role_r = Role::where('id', '=', $role)->firstOrFail();
                $user->assignRole($role_r); //Assigning role to user
            }
        }

        toastr()->success('Data has been Added successfully!');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TeamMember  $teamMember
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agent = TeamMember::findorfail($id);

        $output = array(
            'id' => $agent->id,
            'name' => $agent->name,
            'email' => $agent->email,
            'skype' => $agent->skype,
            'mobile' => $agent->mobile,
            'department_id' => $agent->department_id
        );
        return response()->json($output);

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TeamMember  $teamMember
     * @return \Illuminate\Http\Response
     */
    public function edit(TeamMember $teamMember)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TeamMember  $teamMember
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $user = Auth::user();
        if(!$user->has('Edit Team Member')){
            toastr()->warning('No permission!');
            return back();
        }
        TeamMember::findorfail($id)->update($request->all());
        return View('admin.organization.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TeamMember  $teamMember
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teamMember = TeamMember::find($id);
        $teamMember->user()->delete();
        $teamMember->delete();
        return back();
    }
}
