<?php

namespace App\Http\Controllers;

use App\Service;
use App\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if($user->hasRole('Admin')){
            $teams = Team::all();
        }else{
            $teams = Team::whereId($user->teamMember->team->id)->get();
        }

        $services = Service::all();

        return view('admin.teams.index', compact('teams','services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $team = Team::where('name', $request->name);
        if($request->organization_id && $request->department_id){
            $team = $team->where('organization_id', $request->organization_id)->where('department_id', $request->department_id);
        }
        $team = $team->get();
        if(count($team) > 0){
            toastr()->info('Team name Already Exist!');
            return back();
        }
        $team = Team::create($request->all());
        if($request->services){
            $team->services()->sync($request->services);
        }
        toastr()->success('Data has been Added successfully!');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Team::find($id);
        $output = array(
            'id' => $data->id,
            'name' => $data->name,
        );
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        Team::findorfail($id)->update($request->all());
        toastr()->success('Data has been Updated successfully!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Team::destroy($id);
        return back();
    }
}
