<?php

namespace App\Http\Controllers;

use App\Service;
use App\Type;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();
        $types = Type::orderBy('created_at','desc')->paginate(15);
        return view('admin.sub-services.index',compact('types','services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $count = Type::where('title',$request->only('title'))->where('service_id',$request->only('service_id'))->first();
        if(!is_null($count) > 0){
            toastr()->info('Sub Service Already Exist!');
            return back();
        }
        Type::create($request->only('title','service_id'));
        toastr()->success('Sub Service Saved successfully');
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $type = Type::find($id);

        if(!is_null($type)){
            $type->delete();
            toastr()->success('Deleted Successfully');
        }else{
            toastr()->warning('Sub Service Not Found');
        }

        return back();


    }

}
