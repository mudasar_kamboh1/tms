<?php

namespace App\Http\Controllers;

use App\Organization;
use App\Service;
use App\Team;
use App\Transformers\ServicesTransformer;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();
//        $data = Service::all();
//        $order = \App\Service::find(1);
//        $relatedOrders = $order->related_orders->first();
        return view('admin.addservices.index',compact('services'));
    }

    public function getServiceTeams($id){
        $service = Service::find($id);
        if(!is_null($service)){
            $teams = $service->teams;
            $types = $service->types;
//            $services = fractal($services, new ServicesTransformer)->serializeWith(new \Spatie\Fractalistic\ArraySerializer())->toArray();
            return response()->json([
                'status' => true,
                'teams' => $teams,
                'types' => $types
            ]);
        }else{
            return response()->json([
                'status' => false,
                'teams' => false,
                'types' => false
            ]);
        }
    }


    public function showTeamAndServices(){
        $teams = Team::all();
        $services = Service::all();
        $teamsServices = Team::whereHas('services')->get();
        return view('admin.services.index',compact('services','teams','teamsServices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $services = Service::create($request->all());
         return redirect()->back();
    }

    public function storeTeamsServices(Request $request)
    {
         $team = Team::find($request->team);
         $team->services()->sync($request->services);
         toastr()->success('successfully saved!');
         return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        dd('show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::findorfail(id);
        return redirect()->back();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        dd($id);

        $service = Service::findorfail($id);
        $service->delete();
        return redirect()->back();

    }

    public function deleteTeamServices($id)
    {
        $team = Team::findorfail($id);
        $team->services()->sync([]);
        toastr()->success('Deleted Successfully');
        return redirect()->back();
    }
}
