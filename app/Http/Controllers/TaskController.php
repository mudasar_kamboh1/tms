<?php

namespace App\Http\Controllers;

use App\Department;
use App\Media;
use App\Organization;
use App\Service;
use App\Status;
use App\Task;
use App\TaskTodoList;
use App\Team;
use App\Type;
use App\User;
use Carbon\Carbon;
use function GuzzleHttp\Promise\is_fulfilled;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
//        if($user->hasRole('Manager') | $user->hasRole('Agent')){
            $assignedTasks = $user->teamMember->assignedTasks()->with('task')->get()->pluck('task');
            $assignedTasksToOther = $user->teamMember->assignTasks()->whereHas('task')->get()->unique('task_id')->pluck('task');

//            dd($assignedTasksToOther);
//        }else{
//
//        }

        return view('admin.task-management.index', compact('assignedTasks','assignedTasksToOther'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = Service::all();
        return view('admin.task-management.add-task', compact('services'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
                $user = Auth::user();
                $teamMember = $user->teamMember;
                //Task Creating Section
                $taskInput = $request->only('title','description');
                $taskInput['slug'] = makeSlug($taskInput['title'], new Task());
                $taskInput['expected_data'] = Carbon::parse($request->dateTime)->format('Y-m-d H:i:s');
                $task = Task::create($taskInput);


                $status = Status::find(1);
                if(!is_null($status)){
                    $task->status()->associate($status)->save();
                }

                // TaskTodo List Adding Section
                $toDo = $request->toDo;
                $toDoTitle = $request->toDoTitle;
                if($toDoTitle){
                    for($i = 0; $i <= (count($toDoTitle) - 1) ; $i++ ){
                        if(isset($toDoTitle[$i])){
                            $toDoList = TaskTodoList::create([
                                'title' => $toDoTitle[$i],
                                'status' => $toDo[$i]
                            ]);

                            $toDoList->task()->associate($task)->save();
                            $toDoList->addedBy()->associate($teamMember)->save();
                        }
                    }
                }

                // Adding Task Medias
                if($request->file){
                    $filePath = fileUpload($request, 'uploads/medias', $oldFilePath = null);
                    $media = Media::create(['file' => $filePath]);
                    $media->task()->associate($task)->save();
                    $media->addedBy()->associate($user)->save();
                }

                $service = Service::find($request->service);
                $type = Type::find($request->type);
                $team = Team::find($request->team);

                if(is_null($service) || is_null($type) || is_null($team)){
                    $task->delete();
                    toastr()->error('SomeThing is wrong with provided info!');
                    return back()->withInput();
                }

                //task Associating with service type
                $task->type()->associate($type)->save();
                //Getting Teams to assign task
//                $accountManager = $team->teamMembers()->where('account_manager',1)->get();

                $accountManager = $team->teamMembers()->whereHas('user.roles', function ($query){
                    $query->whereName('Manager');
                })->get();

                //Assigning task to account managers
                if(count($accountManager) > 0){
                    foreach ($accountManager as $teamMember){
                        if($user->id != $teamMember->user->id){
                            $taskRoute = $task->routes()->create(['type' => 1]);
                            $taskRoute->assignBy()->associate($user->teamMember)->save();
                            //Assigning to Team member
                            $taskRoute->assignTo()->associate($teamMember)->save();
                        }else{
                            $task->delete();
                            toastr()->error('Team not found to assign task!');
                            return back()->withInput();
                        }
                    }

                    toastr()->success('Successfully created!');
                    return redirect('task');
                }else{
                    $task->delete();
                    toastr()->error('Team not found to assign task!');
                    return back()->withInput();
                }
            dd($task);


        }catch (\Exception $e) {
            dd($e);
            toastr()->error('Something went wring!');
            return back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $user = Auth::user();
        $task = Task::whereSlug($slug)->first();
        $teamMember = $user->teamMember;

        if(is_null($task)){
            toastr()->warning('Task not found');
            return back();
        }
        elseif (!is_null($teamMember->taskAssignToMe($task->id))){
            $statuses = Status::all();
            return view('admin.task-management.account-manager-task-detail', compact('task', 'statuses'));
        }
        elseif (!is_null($teamMember->taskAssignByMe($task->id))){
            return view('admin.task-management.task-detail', compact('task'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{

        }catch (\Exception $e) {
            toastr()->error('Something went wring!');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $task = Task::find($id);
            if(!is_null($task)){
                $task->delete();
                // Deleting old task routes
                $task->routes()->delete();
                // Deleting Task lists
                $task->todoLists()->delete();
                // Deleting Task Medias
                $task->medias()->delete();
                toastr()->success('Successfully Deleted!');
            }else{
                toastr()->error('Task Not Found!');
            }

            return back();
        }catch (\Exception $e) {
            toastr()->error('Something went wring!');
            return back();
        }
    }

    public function changeStatus(Request $request){
        $status = Status::find($request->statusID);
        $user = Auth::user();
        $teamMember = $user->teamMember;

        if(is_null($status)){
            return response()->json([
                'status' => false,
                'msg' => 'Status not found'
            ]);
        }

        $task = Task::find($request->taskID);
        if(is_null($task)){
            return response()->json([
                'status' => false,
                'msg' => 'Task Not Found'
            ]);
        }

        $assignedTask = $teamMember->assignedTasks()->where('task_id',$task->id)->first();
        if(!is_null($assignedTask)){
            $task->status()->associate($status)->save();
            return response()->json([
                'status' => true,
                'msg' => 'Status Update Successfully!'
            ]);
        }else{
            return response()->json([
                'status' => false,
                'msg' => "You don't have permission!"
            ]);
        }
    }

    public function changeTodoStatus(Request $request){
        $toDo = TaskTodoList::find($request->toDoID);
        $user = Auth::user();
        $teamMember = $user->teamMember;

        if(is_null($toDo)){
            return response()->json([
                'status' => false,
                'msg' => 'ToDo not found'
            ]);
        }

        $task = Task::find($request->taskID);
        if(is_null($task)){
            return response()->json([
                'status' => false,
                'msg' => 'Task Not Found'
            ]);
        }
        //Checking The User has permission to do
        $assignedTask = $teamMember->assignedTasks()->where('task_id',$task->id)->first();
        if(!is_null($assignedTask)){
            $toDo->update(['status' => $request->status]);
            return response()->json([
                'status' => true,
                'msg' => 'Status Update Successfully!'
            ]);
        }else{
            return response()->json([
                'status' => false,
                'msg' => "You don't have permission!"
            ]);
        }
    }
}
