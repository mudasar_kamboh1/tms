<?php

namespace App\Http\Controllers;

use App\Team;
use App\TeamMember;
use Illuminate\Http\Request;

use App\User;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;


class UserController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //Get all users and pass it to the view
        $user = Auth::user();
        // Just for security purpose
        if($user->hasRole('Admin')){
            $users = User::all();
        }else{
            //Getting Logged in user team
            $teams = $user->TeamMember->team;
            $users = $teams->teamMembers()->with('user')->get();
            $users = $users->pluck('user');
        }
        // Just for security purpose
        return view('users.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //Get all roles and pass it to the view
        $user = Auth::user();
        if($user->hasRole('Admin')){
            $teams = Team::get();
        }else{
            //Getting Logged in user team
            $teams = Team::whereId($user->teamMember->team->id)->get();
        }
        $roles = Role::where('name','!=', 'Admin')->get();
        return view('users.create', compact('teams', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //Validate name, email and password fields
        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users',
//            'password'=>'required|min:6|confirmed',
            'team'=>'required|exists:teams,id',
        ]);

        $team = Team::find($request->team);
        $input = $request->only('name','email');

        if($request->password){
            $input['password'] = $request->password;
        }

        $user = User::create($request->only('email', 'name')); //Retrieving only the email and password data

        $teamMember = TeamMember::create([]);
        // Saving Team member as well
        $teamMember->user()->associate($user)->save();
        $teamMember->team()->associate($team)->save();
        $roles = $request['roles']; //Retrieving the roles field
        //Checking if a role was selected
        if (isset($roles)) {

            foreach ($roles as $role) {
                $role_r = Role::where('id', '=', $role)->firstOrFail();
                $user->assignRole($role_r); //Assigning role to user
            }
        }
        //Redirect to the users.index view and display message
        toastr()->success('User successfully added.');
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return redirect('users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $user = Auth::user();
        // Just for security purpose
        if($user->hasRole('Admin')){
            $user = User::findOrFail($id); //Get user with specified id
            $teams = Team::get();
        }else{
            //Current Team
            $team = $user->teamMember->team;
            $team = $team->teamMembers()->whereHas('user', function ($query) use ($id){
                $query->whereId($id);
            })->first();

            if(is_null($team)){
                toastr()->warning('No permission!');
                return redirect('users');
            }
            $user = User::findOrFail($id); //Get user with specified id
            //Getting Logged in user team
            $teams = Team::whereId($team->id)->get();
        }
        // Just for security purpose

        $roles = Role::where('name','!=', 'Admin')->get();
        return view('users.edit', compact('user', 'roles','teams')); //pass user and roles data to view

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $user = Auth::user();
        // Just for security purpose
        if($user->hasRole('Admin')){
            $user = User::findOrFail($id); //Get role specified by id
        }else{
            //Current Team
            $team = $user->teamMember->team;
            $team = $team->teamMembers()->whereHas('user', function ($query) use ($id){
                $query->whereId($id);
            })->first();

            if(is_null($team)){
                toastr()->warning('No permission!');
                return redirect('users');
            }
            $user = User::findOrFail($id); //Get user with specified id
        }
        // Just for security purpose

        $teamMember = $user->teamMember;

        //Validate name, email and password fields
        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users,email,'.$id,
            'team'=>'required|exists:teams,id',
//            'password'=>'required|min:6|confirmed'
        ]);


        $team = Team::find($request->team);
        $input = $request->only('name','email');

        if($request->password){
            $input['password'] = $request->password;
        }

        $roles = $request['roles']; //Retreive all roles
        $user->fill($input)->save();

        $teamMember->team()->associate($team)->save();

        if (isset($roles)) {
            $user->roles()->sync($roles);  //If one or more role is selected associate user to roles
        }
        else {
            $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
        }
        return redirect()->route('users.index')
            ->with('flash_message',
                'User successfully edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //Find a user with a given id and delete
        $user = User::findOrFail($id);
        if (!$user->hasRole('Admin')){
            $user->delete();
            return redirect()->route('users.index')
                ->with('flash_message',
                    'User successfully deleted.');
        }else{
            toastr()->warning('Admin can not be delete');
            return redirect()->route('users.index');
        }

    }
}