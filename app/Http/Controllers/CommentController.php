<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $task = Task::find($request->task_id);


        $teamMember = $user->teamMember;
        $assignedTask = $teamMember->assignedTasks()->where('task_id',$task->id)->first();
        $assignTask = $teamMember->assignTasks()->where('task_id',$task->id)->first();

        if(!is_null($assignedTask) || !is_null($assignTask) && !is_null($task)){
            $comment = Comment::create([
                'description' => $request->comment
            ]);

            $comment->commentable()->associate($task)->save();
            $comment->teamMember()->associate($teamMember)->save();

            $comments = $task->comments;
            $comments = view('admin.comment.chat', compact('comments'))->render();

            return response()->json([
                'status' => true,
                'msg' => 'Comment successfully added!',
                'comments' => $comments
            ]);
        }else{
            return response()->json([
                'status' => false,
                'msg' => 'Something wrong with provided info!',
                'comments' => ''
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
