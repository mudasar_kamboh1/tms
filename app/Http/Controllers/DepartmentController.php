<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.department.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $data = Department::all();
        foreach ($data as $item) {
            if ($item->name == $request->name && $item->organization_id == $request->organization_id){
                return ['status'=> false];
            }
        }
        Department::create($request->all());

        return back();

//
//        Department::create($request->all());
//        $name = Department::all();
//        $view = view('admin.department.partials.data',['value' => $name ]);
//        $view = $view->render();
//        return response()->json($view);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Department::find($id);
        $output = array(
        'id' => $data->id,
        'name' => $data->name,
    );
        return response()->json($output);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        $data = Department::all();
        foreach ($data as $org){
            if ($org->name == $request->name){
                toastr()->info('Organization Name Already Exist');
                return back();
            }
        }        Department::findorfail($id)->update($request->all());
        toastr()->success('Data has been Updated successfully!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Department::destroy($id);
        $name = Department::all();
        $view = view('admin.department.partials.data',['value' => $name ]);
        $view = $view->render();
        return response()->json($view);
    }
    //-----For Add team Form
    public function getodep($id){

       $data = Department::all()->where('organization_id',$id);
       return response()->json($data);
    }
}
