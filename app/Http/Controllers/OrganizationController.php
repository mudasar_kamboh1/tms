<?php

namespace App\Http\Controllers;

use App\Organization;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $names = Organization::all();
        return view('admin.organization.index',compact('names'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        dd('in create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Organization::all();
            foreach ($data as $org){
                if ($org->name == $request->name){
                    toastr()->info('Organization Name Already Exist');
                    return back();
                }
            }
        $name = Organization::create($request->all());
        toastr()->success('Data has been Inserted successfully!');

        return back();
//        $view = view('admin.organization.partials.data',['value' => $name ]);
//
//        $view = $view->render();
//
//        return response()->json($view);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Organization::find($id);
        $output = array(
            'id' => $data->id,
            'name' => $data->name,
        );
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function edit(Organization $organization)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $data = Organization::all();
        foreach ($data as $org){
            if ($org->name == $request->name){
                toastr()->info('User another Name');
                return back();
            }
        }
        Organization::findorfail($id)->update($request->all());
        toastr()->success('Data has been Updated successfully!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Organization::destroy($id);
        return back();
    }
}
