<?php

namespace App\Http\Controllers;

use App\Media;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $teamMember = $user->teamMember;

        $task = Task::find($request->taskID);
        if(is_null($task)){
            return response()->json([
                'status' => false,
                'msg' => 'Task Not Found'
            ]);
        }
        //Checking The User has permission to do
        $assignedTask = $teamMember->assignedTasks()->where('task_id',$task->id)->first();
        $assignTask = $teamMember->assignTasks()->where('task_id',$task->id)->first();

        if(!is_null($assignedTask) || !is_null($assignTask) && !is_null($task)){
            //Adding Task Medias
            if($request->keys('file')){
                $filePath = fileUpload($request, 'uploads/medias', $oldFilePath = null);
                $media = Media::create(['file' => $filePath]);
                $media->task()->associate($task)->save();
                $media->addedBy()->associate($teamMember)->save();
                return response()->json([
                    'status' => true,
                    'msg' => 'Status Update Successfully!'
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'msg' => 'File is missing'
                ]);
            }
        }else{
            return response()->json([
                'status' => false,
                'msg' => "You don't have permission to upload medias!"
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function mediaDeleteByTask(Request $request)
    {
        $task = Task::find($request->taskID);
        $media = Media::find($request->mediaID);
        if(is_null($task) || is_null($media)){
            return response()->json([
                'status' => false,
                'msg' => 'Something is missing!'
            ]);
        }

        $find = $task->medias()->whereId($media->id)->first();
        if(!is_null($find)){
            $media->delete();
            return response()->json([
                'status' => true,
                'msg' => 'Successfully Deleted!'
            ]);
        }else{
            return response()->json([
                'status' => false,
                'msg' => 'Something is wrong!'
            ]);
        }



    }
}
