<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!Auth::user()->hasRole('Admin') && !Auth::user()->hasRole('Manager')) //If user does //not have this permission
        {
            toastr()->warning('You do not have permission');
            abort('401');
        }

        return $next($request);
    }
}
