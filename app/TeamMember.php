<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamMember extends Model
{
    protected $table = 'team_members';

    protected $guarded = [];

    public function team()
    {
        return $this->belongsTo('App\Team');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function task()
    {
        return $this->belongsTo('App\Task');
    }
    public function medias()
    {
        return $this->hasMany('App\Media');
    }
    //Which tasks this teamMember to others
    public function assignTasks($count = null){
        $result = $this->hasMany('App\TaskRoute', 'assign_by');
        if(!is_null($count)){
            return $result->count();
        }
        return $result;
    }
    //Which task assigned to this teamMember
    public function assignedTasks($count = null){
        $result =  $this->hasMany('App\TaskRoute', 'assign_to');
        if(!is_null($count)){
            return $result->count();
        }
        return $result;
    }

    public function taskAssignByMe($taskID = null){
        if(!is_null($taskID)){
            return $this->hasMany('App\TaskRoute', 'assign_by')->where('task_id',$taskID)->first();
        }
        return null;
    }

    public function taskAssignToMe($taskID = null){
        if(!is_null($taskID)){
            return $this->hasMany('App\TaskRoute', 'assign_to')->where('task_id',$taskID)->first();
        }
        return null;
    }

}
