<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{

    protected $fillable = [
        'name'
    ];

    public function departments()
    {
        return $this->hasMany('App\Department');
    }

    public function services()
    {
        return $this->belongsToMany('App\Service','service_team');
    }

}
