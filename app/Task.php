<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    protected $guarded = [];


    public function detail(){
        return $this->hasMany('App\TaskDetail');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function medias(){
        return $this->hasMany('App\Media');
    }

    public function routes(){
        return $this->hasMany('App\TaskRoute');
    }

    public function todoLists(){
        return $this->hasMany('App\TaskTodoList');
    }

    public function status(){
        return $this->belongsTo('App\Status');
    }

    public function type(){
        return $this->belongsTo('App\Type');
    }
}
