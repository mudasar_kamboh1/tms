<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskRoute extends Model
{
    use SoftDeletes;

    protected $guarded = [];


    public function task(){
        return $this->belongsTo('App\Task');
    }

    public function assignBy(){
        return $this->belongsTo('App\User','assign_by');
    }

    public function assignTo(){
        return $this->belongsTo('App\TeamMember','assign_to');
    }

    public function team(){
        return $this->belongsTo('App\Team','team_id');
    }
}
