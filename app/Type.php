<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $guarded = [];

    public function service(){
        return $this->belongsTo('App\Service');
    }

    public function tasks(){
        return $this->hasMany('App\Task');
    }
}
