<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskTodoList extends Model
{
    use SoftDeletes;

    protected $guarded = [];


    public function task(){
        return $this->belongsTo('App\Task');
    }

    public function addedBy(){
        return $this->belongsTo('App\User','added_by');
    }

}
