<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
        'name',
        'department_id',
        'organization_id',
    ];



    public function department(){
        return $this->belongsTo('App\Department');
    }
    public function teamMembers(){
        return $this->hasMany('App\TeamMember','team_id');
    }
    public function services(){
        return $this->belongsToMany('App\Service');
    }
}
