<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;

    protected $guarded = [];


    public function commentable()
    {
        return $this->morphTo();
    }

    public function teamMember(){
        return $this->belongsTo('App\TeamMember','added_by');
    }



}
