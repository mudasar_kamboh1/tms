<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Media extends Model
{
    use SoftDeletes;

    protected $table = 'medias';

    protected $guarded = [];


    public function task(){
        return $this->belongsTo('App\Task');
    }

    public function addedBy(){
        return $this->belongsTo('App\TeamMember','added_by');
    }
}
