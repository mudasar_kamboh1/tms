<?php

namespace App\Transformers;

use App\Service;
use League\Fractal\TransformerAbstract;

class ServicesTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Service $service)
    {
        return [
            'id' => $service->id,
            'title' => $service->title
        ];
    }
}
