<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function tasks(){
        return $this->belongsToMany('App\Task');
    }

    public function teams(){
        return $this->belongsToMany('App\Team');
    }

    public function types(){
        return $this->hasMany('App\Type');
    }
}
