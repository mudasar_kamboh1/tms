<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskDetail extends Model
{
    use SoftDeletes;

    protected $guarded = [];

}
