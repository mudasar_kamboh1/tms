<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $guarded = [];


    public function organization()
    {
        return $this->belongsTo('App\Organization');
    }

    public function teams()
    {
        return $this->hasMany('App\Team');
    }

}
