<?php

use Illuminate\Http\Request;



if (!function_exists('fileUpload')) {

    function fileUpload(Request $request, $path, $oldImagePath = null) {
        if(!is_null($oldImagePath)){
            if (file_exists($oldImagePath)) {
                @unlink($oldImagePath);
            }
        }
        $file = $request->file('file');
        $filename =  time() . '.' . $file->getClientOriginalExtension();
        $file->move($path,$filename);
        return $filename;
    }
}

if (!function_exists('makeSlug')) {

    function makeSlug($title, $model) {
        $slug = str_slug($title,'-');
        $slugCount = count($model->whereRaw("slug REGEXP '^{$slug}(-[0-9]*)?$'")->get());
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }
}
