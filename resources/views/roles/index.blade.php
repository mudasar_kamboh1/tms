@extends('admin.layouts.app')

@section('title', '| Users')

@section('content')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2><a href="{{ url()->previous() }}" class="btn btn-xs btn-link"><i
                                        class="fa fa-arrow-left"></i></a>Roles</h2>
                        <ul class="breadcrumb">

                        </ul>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="body">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Role</th>
                                            <th>Permissions</th>
                                            <th>Operation</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach ($roles as $role)
                                            <tr>

                                                <td>{{ $role->name }}</td>

                                                <td>{{ str_replace(array('[',']','"'),'', $role->permissions()->pluck('name')) }}</td>{{-- Retrieve array of permissions associated to a role and convert to string --}}
                                                <td>
                                                    <a href="{{ URL::to('roles/'.$role->id.'/edit') }}" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>

                                                    {!! Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id] ]) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                                    {!! Form::close() !!}

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                </div>

                                <a href="{{ URL::to('roles/create') }}" class="btn btn-success">Add Role</a>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection