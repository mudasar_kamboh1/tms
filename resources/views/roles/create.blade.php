@extends('admin.layouts.app')

@section('title', '| Add Role')

@section('content')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2><a href="{{ url()->previous() }}" class="btn btn-xs btn-link"><i
                                        class="fa fa-arrow-left"></i></a> Add Role</h2>
                        <ul class="breadcrumb">

                        </ul>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="body">
                            <div class='col-lg-4 col-lg-offset-4'>

                                {{ Form::open(array('url' => 'roles')) }}

                                <div class="form-group">
                                    {{ Form::label('name', 'Name') }}
                                    {{ Form::text('name', null, array('class' => 'form-control')) }}
                                </div>

                                <h5><b>Assign Permissions</b></h5>

                                <div class='form-group'>
                                    @foreach ($permissions as $permission)
                                        {{ Form::checkbox('permissions[]',  $permission->id ) }}
                                        {{ Form::label($permission->name, ucfirst($permission->name)) }}<br>

                                    @endforeach
                                </div>

                                {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

                                {{ Form::close() }}

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection