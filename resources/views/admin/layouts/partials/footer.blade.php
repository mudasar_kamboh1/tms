{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>--}}
{{--<script src="{{ asset('/assets/js/pages/tables/jquery-datatable.js') }}"></script>--}}
{{--<script src="{{ asset('/assets/js/pages/ui/dialogs.js') }}"></script>--}}
<script src="{{ asset('/assets/bundles/libscripts.bundle.js') }}"></script>
<script src="{{ asset('/assets/bundles/vendorscripts.bundle.js') }}"></script>
<script src="{{ asset('/assets/bundles/mainscripts.bundle.js') }}"></script>
{{--<script src="{{ asset('/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>--}}
<script src="{{ asset('/assets/js/chosen.jquery.js') }}"></script>
<script src="{{ asset('/assets/js/prism.js') }}"></script>
<script src="{{ asset('/assets/js/init.js') }}"></script>
<script src="{{ asset('/assets/js/bootstrap-datetimepicker.js') }}"></script>
<script>
    $('.form_datetime').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        startDate: '-0d',
        todayBtn: 1,
        format: 'dd-mm-yyyy H:iiP',
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1
    });
</script>
<script src="{{ asset('/assets/js/todo.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.validate.js') }}"></script>
<script>
    $("#add-task-form").validate({
        rules: {
            title: {
                required: true,
            },
            dateTime: {
                required: true,
            },
            organization: {
                required: true,
            },
            service: {
                required: true,
            },

            file: {
                required: true,
            },
        },
        messages: {
            title: "Please enter Task title",

            dateTime: "Please enter Task Completion Date",

            organization: "Please select Organization",

            service: "Please select service",

            file: "Please Add media files",
        }
    });
</script>
{{--<script src="{{ asset('/assets/vendor/sweetalert/sweetalert.min.js') }}"></script> <!-- SweetAlert Plugin Js -->--}}
{{--<script src="{{ asset('/assets/js/index.js') }}"></script>--}}
{{--<script src="{{ asset('/assets/js/app.js') }}"></script>--}}

{{--<script src="{{ asset('/assets/js/jquery.dataTables.js') }}"></script>--}}
{{--<script src="{{ asset('/assets/code.jquery.com/ui/1.12.1/jquery-ui.js') }}"></script>--}}


{{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
{{--<script src="assets/bundles/vendorscripts.bundle.js"></script>--}}

{{--<script src="../assets/vendor/table-dragger/table-dragger.min.js"></script>--}}

{{--<script src="assets/bundles/mainscripts.bundle.js"></script>--}}
