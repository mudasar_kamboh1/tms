<div class="sidebar-scroll">
    <div class="user-account">
        <img src="../assets/images/user.png" class="rounded-circle user-photo" alt="User Profile Picture">
        <div class="dropdown">
            <span>
                {{ (count(\Auth::user()->getRoleNames()) > 0) ? \Auth::user()->getRoleNames()[0] : '--' }}

            </span>
            <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong>{{ \Auth::user()->name }}</strong></a>
            <ul class="dropdown-menu dropdown-menu-right account animated flipInY">
                <li><a href="#"><i class="icon-user"></i>My Profile</a></li>
                <li class="divider"></li>
                <li><a href="{{ url('/logout') }}"><i class="icon-power"></i>Logout</a></li>
            </ul>
        </div>
        <hr>

    </div>

            <nav class="sidebar-nav">
                <ul class="main-menu metismenu">
                    @can('Show Task Menu')
                        <li><a href="{{ url('task') }}" ><i class="material-icons">line_style</i><span>Task</span></a></li>
                    @endcan
                    @can('Show Team Menu')
                        <li><a href="{{ url('team') }}" ><i class="material-icons">people</i><span>Team</span></a></li>
                    @endcan

                    @can('Show Settings Menu')
                    <li>
                        <a href="#" class="has-arrow"><i class="material-icons">settings</i><span>Settings</span></a>
                        <ul>
                            @can('Show Organization Menu')
                                <li><a href="{{ url('organization') }}"><i class="material-icons">view_array</i><span>Organizations</span></a></li>
                            @endcan
                            @can('Show Services Menu')
                                <li><a href="{{ url('addservices') }}"><i class="material-icons">explore</i><span> Services</span></a></li>
                            @endcan
                            @can('Show Sub Services Menu')
                                <li><a href="{{ url('type') }}" ><i class="material-icons">person_add</i><span>Sub Services</span></a></li>
                            @endcan
                            @can('Show Team Services Menu')
                                <li><a href="{{ url('show-team-serviecs') }}"><i class="material-icons">gamepad</i><span>Team Services</span></a></li>
                            @endcan
                            @hasrole('Admin')
                                <li><a href="{{ url('permissions') }}" ><i class="material-icons">warning</i><span>Permissions</span></a></li>
                                <li><a href="{{ url('roles') }}" ><i class="material-icons">stop_screen_share</i><span>Roles</span></a></li>
                                <li><a href="{{ url('users') }}" ><i class="material-icons">person_add</i><span>Users</span></a></li>
                            @endhasrole
                        </ul>
                    </li>
                    @endcan
                </ul>
            </nav>
    </div>

</div>
