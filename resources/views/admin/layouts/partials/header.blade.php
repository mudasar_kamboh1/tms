<title>{{ config('app.name', 'Laravel') }}</title>
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Lucid Bootstrap 4.1.1 Admin Template">
<meta name="author" content="WrapTheme, design by: ThemeMakker.com">
<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">
<!-- VENDOR CSS -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

{{--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">--}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
{{--<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">--}}
<link rel="stylesheet" href="{{ asset('/assets/css/chosen.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/css/prism.css') }}">
{{--<link rel="stylesheet" href="{{ asset('/assets/css/todos.css') }}">--}}
<link rel="stylesheet" href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}">
{{--<link rel="stylesheet" href="{{asset('assets/vendor/sweetalert/sweetalert.css')}}">--}}
{{--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">--}}
<!-- MAIN CSS -->
<link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/color_skins.css')}}">

<!-- Styles -->
{{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
