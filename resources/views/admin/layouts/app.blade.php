<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <!-- Scripts -->
    @include('admin.layouts.partials.header')
        {{--<script src="{{ asset('assets/js/app.js') }}" defer></script>--}}
    @toastr_css
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
</head>
<body class="theme-orange" style="background-image: url({{ asset('uploads/medias/auth_bg.jpg') }});">

<div id="wrapper">
    <nav class="navbar navbar-fixed-top">
        @include('admin.layouts.partials.nav')
    </nav>

    <div id="left-sidebar" class="sidebar">
        @include('admin.layouts.partials.side')

    </div>
    @yield('content')


</div>
@include('admin.layouts.partials.footer')

@yield('js')
</body>
    {{--@jquery--}}
    @toastr_js
    @toastr_render

    <script>
        @if(count($errors) > 0)
            @foreach($errors->all() as $error)
    toastr.error("{{ $error }}");
        @endforeach
        @endif
    </script>
</html>
