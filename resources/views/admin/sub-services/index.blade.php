@extends('admin.layouts.app')

@section('content')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2><a href="{{ url()->previous() }}" class="btn btn-xs btn-link"><i
                                        class="fa fa-arrow-left"></i></a>Sub Services</h2>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="card">
                    <div class="body">
                        <form action="{{ route('type.store') }}" method="post">
                            @csrf
                            <div class="row clearfix">
                                 <div class="col-md-4 col-sm-12">
                                    <label>Service</label>
                                    <select data-placeholder="Choose Services..." id="services" class="chosen-select" tabindex="2" name="service_id" required  data-height="40px">
                                        <option value="">Select Services</option>
                                        @foreach($services as $service)
                                            <option value="{{ $service->id }}">{{ $service->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <label class="label">Sub Service Title*</label><br>
                                       <input type="text" name="title" class="form-control" placeholder="Title">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary" style="margin-top: 28px;">Create</button>
                                    </div>
                                </div>
                                 </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row clearfix" id="cardrander">
                <div class="col-lg-12">
                    {{--@can('View Team Services')--}}
                        <div class="card">

                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered m-b-0 c_list">
                                        <thead>
                                        <tr>
                                            <th>Sub service</th>
                                            <th>Service</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($types) > 0)
                                            @foreach($types as $type)
                                                <tr>
                                                    <td>{{ $type->title }}</td>
                                                    <td>
                                                        {{ !is_null($type->service) ? $type->service->title : ''}}
                                                    </td>
                                                    <td>
                                                        @can('Delete Sub Services')
                                                            <form action="{{ route('type.destroy', $type->id) }}" method="post">
                                                                @method('DELETE')
                                                                @csrf
                                                                <button  type="submit" class="btn btn-danger js-sweetalert" title="Delete"><i class="fa fa-trash-o"></i></button>
                                                            </form>
                                                        @endcan
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="3"> <h6 class="text-center text-warning">Record Not Found!</h6></td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    {{--@endcan--}}
                    {{ $types->render() }}
                </div>
            </div>
        </div>
    </div>


@endsection

