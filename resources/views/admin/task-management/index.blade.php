@extends('admin.layouts.app')

@section('content')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2><a href="{{ url()->previous() }}" class="btn btn-xs btn-link"><i
                                        class="fa fa-arrow-left"></i></a>Task Manager</h2>
                        <ul class="breadcrumb">

                        </ul>
                    </div>
                    @can('Create Task')
                        <div class="col-lg-6 col-md-8 col-sm-12 ">
                            <a href="{{ route('task.create') }}" class="btn btn-info btn-lg pull-right" >Generate New Task</a>
                        </div>
                    @endcan
                </div>

            </div>
            @can('View Task')
                <div class="row clearfix" id="cardrander">
                <div class="col-lg-12">
                    <div class="card">

                        <div class="body">
                            <h5>Task Assigned To me</h5>
                            <div class="table-responsive">
                                        <table class="table table-hover m-b-0 c_list">
                                            <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Type</th>
                                                <th>Assign By</th>
                                                <th>Assign To</th>
                                                <th>Deadline</th>
                                                <th>Create Date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @if(count($assignedTasks))
                                                    @foreach($assignedTasks as $task)
                                                        <tr>
                                                            <td>{{ $task->title }}</td>
                                                            <td>{{ !is_null($task->type) ? $task->type->title : 'N/A'  }}</td>
                                                            <td>{{ !is_null($task->routes()->first()) ?  $task->routes()->first()->assignBy->name : 'N/A' }}</td>
                                                            <td>{{ !is_null($task->routes()->first()) && !is_null($task->routes()->first()->assignTo->user) ? $task->routes()->first()->assignTo->user->name : 'N/A'}}</td>
                                                            <td>{{ !is_null($task->expected_data) ? \Carbon\Carbon::parse($task->expected_data)->format('h:i A d-m-Y') : '' }}</td>
                                                            <td>{{ !is_null($task->created_at) ? \Carbon\Carbon::parse($task->created_at)->format('h:i A d-m-Y') : '' }}</td>
                                                            <td><button class="btn btn-warning">{{ !is_null($task->status) ? $task->status->title : 'N/A'  }}</button></td>
                                                            <td class="text-center">
                                                                <a href="{{ route('task.show', $task->slug) }}" class="btn btn-info" title="View"><i class="fa fa-eye"></i></a>
                                                                @can('Delete Task')
                                                                <hr>
                                                                <form action="{{ route('task.destroy', $task->id) }}" method="post">
                                                                    @method('DELETE')
                                                                    @csrf
                                                                    <button  type="submit" class="btn btn-danger js-sweetalert" title="Delete"><i class="fa fa-trash-o"></i></button>
                                                                </form>
                                                                @endcan
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr><td colspan="8"><span class="label label-info">No Task Assigned To You Yet</h5></td></tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="card">
                        <div class="body">
                            <h5>Task Assigned By me</h5>
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0 c_list">
                                    <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Type</th>
                                        <th>Assign By</th>
                                        <th>Assign To</th>
                                        <th>Deadline</th>
                                        <th>Create Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($assignedTasksToOther))
                                        @foreach($assignedTasksToOther as $task)
                                            <tr>
                                                <td>{{ $task->title }}</td>
                                                <td>{{ !is_null($task->type) ? $task->type->title : 'N/A'  }}</td>
                                                <td>{{ !is_null($task->routes()->first()) ?  $task->routes()->first()->assignBy->name : 'N/A' }}</td>
                                                <td>{{ !is_null($task->routes()->first()) && !is_null($task->routes()->first()->assignTo->user) ? $task->routes()->first()->assignTo->user->name : 'N/A'}}</td>
                                                <td>{{ !is_null($task->expected_data) ? \Carbon\Carbon::parse($task->expected_data)->format('h:i A d-m-Y') : '' }}</td>
                                                <td>{{ !is_null($task->created_at) ? \Carbon\Carbon::parse($task->created_at)->format('h:i A d-m-Y') : '' }}</td>
                                                <td><button class="btn btn-warning">{{ !is_null($task->status) ? $task->status->title : 'N/A'  }}</button></td>
                                                <td class="text-center">
                                                    {{--<a href="" class="btn btn-info" title="Edit"><i class="fa fa-edit"></i></a>--}}
                                                    <a href="{{ route('task.show', $task->slug) }}" class="btn btn-info" title="View"><i class="fa fa-eye"></i></a>
                                                    @can('Delete Task')
                                                        <hr>
                                                        <form action="{{ route('task.destroy', $task->id) }}" method="post">
                                                            @method('DELETE')
                                                            @csrf
                                                            <button  type="submit" class="btn btn-danger js-sweetalert" title="Delete"><i class="fa fa-trash-o"></i></button>
                                                        </form>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr><td colspan="8"><span class="label label-info">No Task Assigned By You Yet</span></td></tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endcan
        </div>
    </div>

@endsection
