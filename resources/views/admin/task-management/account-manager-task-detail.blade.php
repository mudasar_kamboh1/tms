@extends('admin.layouts.app')

@section('content')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2><a href="{{ url()->previous() }}" class="btn btn-xs btn-link"><i
                                        class="fa fa-arrow-left"></i></a>Task Manager</h2>
                        <ul class="breadcrumb">

                        </ul>
                    </div>
                </div>
            </div>
            @can('View Task')
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="body">
                                <h5>{{ $task->title }}</h5>
                                <p>{{ $task->description }}</p>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-12">
                                        <div class="body">
                                            <ul class=" list-unstyled basic-list row">
                                                <li class="col-md-6">Create On:<span class="badge badge-primary">{{ !is_null($task->created_at) ? \Carbon\Carbon::parse($task->created_at)->format('h:i A d-m-Y') : '' }}</span></li>
                                                <li class="col-md-6">Deadline:<span class="badge-purple badge">{{ !is_null($task->expected_data) ? \Carbon\Carbon::parse($task->expected_data)->format('h:i A d-m-Y') : '' }}</span></li>
                                                <li class="col-md-6">Create By:<span class="badge-purple badge">{{ !is_null($task->routes()->first()) ?  $task->routes()->first()->assignBy->name : '-- -- --' }}</span></li>
                                                <li class="col-md-6">Assign to:<span class="badge-danger badge">{{ !is_null($task->routes()->first()) && !is_null($task->routes()->first()->assignTo->user) ? $task->routes()->first()->assignTo->user->name : 'N/A'}}</span></li>
                                                <li class="col-md-6">Status <button class="btn btn-info pull-right" style="background-color: {{ config('constants.STATUSES.'.strtolower($task->status->title)) }}">{{ !is_null($task->status) ? $task->status->title : 'N/A'  }}</button></li>
                                                <li class="col-md-6">
                                                    @can('Set Status Of The Task')
                                                        <select class="form-control show-tick" id="change-status">
                                                            <option value="">Select Status</option>
                                                            @if(count($statuses))
                                                                @foreach($statuses as $status)
                                                                    <option value="{{ $status->id }}" {{ !is_null($task->status) && ($task->status->id == $status->id) ? ' selected': '' }}>{{ $status->title }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    @endcan
                                                </li>
                                            </ul>
                                            <br>
                                            <h6></h6>

                                        </div>

                                    </div>
                                    <div class="col-lg-6 col-md-12">


                                        <div class="body todo_list">
                                            <h5>ToDo List</h5>
                                            <ul class="list-unstyled mb-0">
                                                @if(count($task->todoLists) > 0)
                                                    @foreach($task->todoLists as $todo)
                                                        <li>
                                                            <label class="fancy-checkbox mb-0">
                                                                <input type="checkbox" data-id="{{ $todo->id }}" class="todo-status" name="checkbox" {{ $todo->status == 1 ? 'checked' : '' }}>
                                                                <span>{{ $todo->title }}</span>
                                                            </label>
                                                            <hr>
                                                        </li>
                                                    @endforeach
                                                @else
                                                    <h6 class="text-center text-danger">No, ToDO added yet!</h6>
                                                @endif
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix" style="display: none;">
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="card">
                                            <div class="body">
                                        <div class="header">
                                            <h2>Team Members</h2>
                                        </div>
                                        <div class="body">
                                            <ul class="list-group basic-list">
                                                <li class="d-flex justify-content-between align-items-center">
                                                    Cras justo odio<span class="badge badge-warning">14</span>
                                                </li>
                                                <li class="d-flex justify-content-between align-items-center">
                                                    Dapibus ac facilisis in<span
                                                            class="badge badge-success">2</span></li>
                                                <li class=" d-flex justify-content-between align-items-center">
                                                    Morbi leo risus<span class="badge badge-info">1</span>
                                                </li>
                                                <li class="d-flex justify-content-between align-items-center">
                                                    Cras justo odio<span class="badge badge-primary">14</span>
                                                </li>
                                                <li class="d-flex justify-content-between align-items-center">
                                                    Dapibus ac facilisis in<span
                                                            class="badge badge-danger">2</span></li>
                                            </ul>
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="add-task">
                            <div class="row clearfix">
                                <div class="col-12 media-section">
                                    <h6>Client Media</h6>
                                    @can('Create Media')
                                    <div class="card">
                                        <div class="card-body">
                                            Select File to upload:
                                            <input type="file" name="file" id="file">
                                            <button class="btn btn-warning" id="fileToUpload">Click To Upload</button>
                                        </div>
                                    </div>
                                    @endcan
                                </div>
                                @can('View Media')
                                    @if(count($task->medias) > 0)
                                        @foreach($task->medias as $media)
                                            <div class="col-md-4 p-3 media-list-box">
                                                <div class="card media-list">
                                                    <button type="button" data-id="{{ $media->id }}" class="btn btn-danger media-del-btn"><i class="fa fa-trash"></i></button>
                                                    <a href="{{ asset('uploads/medias/'.$media->file) }}" target="_blank">
                                                        <div class="row clearfix ">
                                                            <div class="card-block col-3">
                                                                @if (pathinfo(asset('uploads/medias/'.$media->file), PATHINFO_EXTENSION) == 'png' || pathinfo(asset('uploads/medias/'.$media->file), PATHINFO_EXTENSION) == 'jpg')
                                                                    <img class="w-100" height="70px" src="{{ asset('uploads/medias/'.$media->file) }}" >
                                                                @else
                                                                    <img class="w-100" src="https://pngimage.net/wp-content/uploads/2018/06/file-png-12.png" >
                                                                @endif
                                                            </div>
                                                            <div class="card-block col-9">
                                                                <br>
                                                                <h6 class="card-text pull-left" style="padding:10px 0 10px 10px;color:#000;">{{ !is_null($media->addedBy) ? $media->addedBy->user->name : '' }}</h6>
                                                                <p class="card-text pull-right" style="padding:10px 10px 10px 0;color:#000;">{{ !is_null($media->created_at) ? \Carbon\Carbon::parse($media->created_at)->format('h:i A d-m-Y') : '' }}</p>
                                                            </div>
                                                        </div>

                                                    </a>

                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <h6 class="col-md-12 text-center text-danger">No, Media added yet!</h6>
                                    @endif
                                @endcan

                            </div>
                            <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12">
                                        @can('Read Comment')
                                            <div class="card br-0 mb-0">
                                            <div class="body" id="comment-chat">
                                                <h5>Recent Comments</h5>
                                                <div class="card-height">
                                                    @if(count($task->comments))
                                                        @foreach($task->comments as $comment)
                                                            <div class="{{ $comment->teamMember && (\Illuminate\Support\Facades\Auth::user()->id == $comment->teamMember->user->id) ? 'warning' : 'green'}} timeline-item text-left clearfix">
                                                                <span class="date"> {{ \Carbon\Carbon::parse($comment->created_at)->format('h:i A d-M-y') }}</span>
                                                                <h6> {{ $comment->description }}</h6>
                                                                <span>Added By: <a href="javascript:void(0);" title="Team Member">
                                                {{ !is_null($comment->teamMember) && !is_null($comment->teamMember->user) ? $comment->teamMember->user->name : 'N/A' }}
                                            </a></span>
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        @endcan
                                        @can('Create Comment')
                                        <div class="card br-0">
                                            <div class="header">
                                                <h2>Post Comment</h2>
                                            </div>
                                            <div class="body">
                                                <form method="POST" action="#" id="comments">
                                                    <div class="input-group mb-3">
                                                        <input type="hidden" name="task_id" value="{{ $task->id }}">
                                                        <textarea type="text" name="comment" id="comment" class="form-control" placeholder="Post Your Comment" aria-label="Post Comment" aria-describedby="basic-addon2"></textarea>
                                                        <div class="input-group-append">
                                                            <button class="btn btn-outline-secondary" type="submit"><i class="icon-paper-plane"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        @endcan
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endcan
        </div>
    </div>
@endsection


@section('js')
    <script>
        $(document).ready(function () {
            $('#comments').on('submit',function (e) {
                e.preventDefault();
                var $this = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('comment.store') }}",
                    data : $this.serialize(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (result) {
                        if(result.status){
                            $('#comment-chat').html(result.comments);
                            $('.card-height').scrollTop($('.card-height')[0].scrollHeight);
                            $($this)[0].reset();
                            toastr.info(result.msg);
                        }else{
                            toastr.error(result.msg);
                        }
                    }
                });
            });

            $('#comment').keypress(function (e) {
                if (e.which == 13) {
                    if($(this).val() != ''){
                        $('#comments').submit();
                    }
                    return false;
                }
            });

            $('#change-status').on('change',function () {
                var $this = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ url('change-status') }}",
                    data : { statusID : $this.val(), taskID: "{{$task->id}}" } ,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (result) {
                        if(result.status){
                            toastr.info(result.msg);
                            location.reload();
                        }else{
                            toastr.error(result.msg);
                        }
                    }
                });
            });

            $('.media-del-btn').on('click',function () {
                var $this = $(this);
                $.ajax({
                    type: 'DELETE',
                    url: "{{ url('mediaDeleteByTask') }}",
                    data : { mediaID : $this.data('id'), taskID: "{{$task->id}}" } ,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (result) {
                        if(result.status){
                            toastr.info(result.msg);
                            $this.parents('.media-list-box').remove();
                        }else{
                            toastr.error(result.msg);
                        }
                    }
                });
            });

            $('.todo-status').on('change',function () {
                var $this = $(this);
                var status;
                if($this.prop("checked") == true){
                    status = 1;
                }
                else if($this.prop("checked") == false){
                    status = 0;
                }
                $.ajax({
                    type: 'POST',
                    url: "{{ url('changeTodoStatus') }}",
                    data : { toDoID : $this.data('id'), status: status, taskID: "{{$task->id}}" } ,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (result) {
                        if(result.status){
                            toastr.info(result.msg);
                            location.reload();
                        }else{
                            toastr.error(result.msg);
                        }
                    }
                });
            });

        });


        $('#fileToUpload').on('click',function () {
            if ($('#file').val() != '') {
                upload(document.getElementById('file'));
            }else{
                toastr.error('Please select file.');
            }
        });


        function upload(img) {
            var form_data = new FormData();
            form_data.append('file', img.files[0]);
            form_data.append('taskID', '{{$task->id}}');
            $('#loading').css('display', 'block');
            $.ajax({
                url: "{{route('media.store')}}",
                data: form_data,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: false,
                processData: false,
                success: function (result) {
                    if(result.status){
                        toastr.info(result.msg);
                        location.reload();
                    }else{
                        toastr.error(result.msg);
                    }
                },
                error: function (xhr, status, error) {
                    toastr.error(xhr.responseText);
                }
            });
        }


    </script>
@endsection