@extends('admin.layouts.app')

@section('content')


    {{--{{ dd(old()) }}--}}
<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-6 col-md-8 col-sm-12">
                    <h2><a href="{{ url()->previous() }}" class="btn btn-xs btn-link"><i
                                    class="fa fa-arrow-left"></i></a>Add Task</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('task') }}"><i class="icon-home"></i></a></li>
                        <li class="breadcrumb-item active">Task</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="">
<div class="contaner">
    @can('Create Task')
        <form class="" action="{{ route('task.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="add-task">
        <div class="row clearfix">

            <div class="col-6">
                <div class="form-group">
                    <label class="label-default">Title*</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" value="{{ old('title') }}" required>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label class="label-default">Expected Date | Deadline*</label>
                    <div class="controls input-append date form_datetime " data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1" >
                        <input size="16" class="form-control" value="{{ old('dateTime') }}"  name="dateTime" type="text" placeholder="Task Complete date" autocomplete="off" required>
                        <span class="add-on"><i class="icon-remove"></i></span>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-4">
                <div class="form-group">
                    <label class="label-default">Services*</label>
                    <select data-placeholder="Select Service" id="service" name="service" class="chosen-select" tabindex="2" required>
                        <option value="">Please Select</option>
                        @foreach($services as $service)
                            <option value="{{ $service->id }}" {{ old('service') == $service->id ? 'selected' : '' }}>{{ $service->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <label class="label-default">Service Types*</label>
                <select name="type" data-placeholder="Select Type..." id="type" class="chosen-select" tabindex="2" required>
                    @if(old('type'))
                        {{ $type = \App\Type::find(old('type')) }}
                        <option value="{{ $type->id }}">{{ $type->title }}</option>
                    @else
                        <option value="">Please Select</option>
                    @endif

                </select>

            </div>
            <div class="col-4">
                <label class="label-default">Service's Teams*</label>
                <select name="team" data-placeholder="Select Team..." id="teams" class="chosen-select" tabindex="2" required>
                    @if(old('service'))
                        {{ $type = \App\Service::find(old('service')) }}
                        <option value="{{ $type->id }}">{{ $type->title }}</option>
                    @else
                        <option value="">Please Select</option>
                    @endif
                </select>

            </div>
        </div>
            <div class="row clearfix">
                <div class="col-12">
                    <div class="form-group">
                        <label class="label-default">Description</label>
                        <textarea class="form-control" placeholder="Description" name="description">{{ old('description') }}</textarea>
                    </div>
                </div>
        </div>
        </div>
        <div class="add-task">
        <div class="row clearfix">
        <div class="col-12">
            <div class="form-group">
                <label class="label-default">Add Task TODO Lists</label>
                <div id="form-add-todo" class="form-add-todo">
                    <input type="text" id="new-todo-item" class="new-todo-item form-control" name="todo" placeholder="What needs to be done?" />
                    <input type="submit" id="add-todo-item" class="add-todo-item" value="add the todo"  style="display:none"/>
                </div>

                <div id="form-todo-list">
                    <ul id="todo-list" class="todo-list">
                        @if(old('toDoTitle'))
                            @foreach(old('toDoTitle') as $k => $value)
                                <li>
                                    <label class="fancy-label">
                                        <input type="checkbox" name="todo-item-done" class="todo-item-done">
                                        <span class="label-text"></span>
                                    </label><input type="text" name="toDoTitle[]" class="select-item-done lineInput {{ isset(old('toDo')[$k]) && old('toDo')[$k] == 1 ? 'strike' : '' }}" value="{{ isset(old('toDoTitle')[$k]) ? old('toDoTitle')[$k] : '' }}">
                                    <input type="hidden" name="toDo[]" class="todo-item-done" {{ isset(old('toDo')[$k]) && old('toDo')[$k] == 1 ? 'checked  value=1' : '' }}>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>



        </div>
        </div>
        </div>
        <div class="add-task">
        <div class="row clearfix ">
            <div class="col-12 media-section">
                <h6>Add Media</h6>

                <div class="form-group">
                    Select image to upload:
                    <input type="file" name="file" id="fileToUpload" value="{{ old('file') }}">
                </div>
            </div>
        </div>
        </div>
            <button type="submit" class="btn btn-primary btn-lg ">Create Task</button>
            <br>
            <br>

    </form>
    @endcan
    </div>
        </div>
    </div>
</div>
@endsection


@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#service').change(function () {
                var id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: '{{url('getServiceTeams')}}/' + id,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (result) {
                        $('#teams .list, #type .list').remove();
                        $.each(result.teams, function (i, row) {
                            $('#teams').append('<option value="'+row.id+'" class="list">'+row.name+'</option>');
                        });
                        $.each(result.types, function (i, row) {
                            $('#type').append('<option value="'+row.id+'" class="list">'+row.title+'</option>');
                        });
                        $('#teams, #type').trigger("chosen:updated");
                    }
                });
            });
        });
    </script>
@endsection