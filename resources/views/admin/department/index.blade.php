@extends('admin.layouts.app')

@section('content')


    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2><a href="{{ url()->previous() }}" class="btn btn-xs btn-link"><i
                                        class="fa fa-arrow-left"></i></a>Department</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="tms"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item active">Department</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix" id="cardrander">
                @include('admin.department.partials.data')
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="myModal2" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Department</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="post">
                        @csrf
                        <div class="form-group">
                            <input type="text" id="department_name" name="department_name" placeholder="Name" class="form-control" />
                        </div>
                        <div class="form-group">
                            <select id="organization_id" class="custom-select">
                                <option selected>Select Organization</option>
                                @foreach(App\Organization::all() as $data)
                                    <option value="{{$data->id}}"> {{ $data->name }} </option>
                                    @endforeach
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="button" class="btn btn-success" value="Add" onclick="add_department()" data-dismiss="modal"  />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal1 -->
    <div id="myModal3" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update Department</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="post">
                        @csrf
                        <div class="form-group">
                            <input type="text" id="department_name1" name="department_name" placeholder="Name" class="form-control" />
                        </div>
                        <div class="form-group">
                            <select id="organization_name" class="custom-select">
                                <option selected>Select Organization</option>
                                @foreach(App\Organization::all() as $data)
                                    <option value="{{$data->id}}"> {{ $data->name }} </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="button" class="btn btn-success" id="update_dep" value="Update" data-dismiss="modal"  />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    function add_department(){
        $.ajax({
            type : 'post',
            route: 'department222',
            data:{
                '_token':$('input[name=_token]').val(),
                'name':$('input[name=department_name]').val(),
                'organization_id':$('#organization_id option:selected').val(),


    },
            success:function (data) {
                if(data.status == false){
                    toastr.error('Name Already Exist!');
                }else{
                    toastr.success('Data has been added successfully!');
                    $('#cardrander').html('');
                    $('#cardrander').append(data);
                    $('.listings').DataTable();
                }


            }
        });
    }
    //---------del
    function del_dep(id) {
        var r = confirm("Press a button!");
        if (r == true) {
            $.ajax({
                type:'post',
                url: "{{ url('department/') }}/"+id,
                data:{
                    '_token':$('input[name=_token]').val()
                },
                success:function (data) {
                    toastr.success('Data has been deleted successfully!');
                    $('#cardrander').html('');
                    $('#cardrander').append(data);
                    $('.listings').DataTable();
                }
            });
        }
    }
    //-----EDIT------
    function edit_dep(id) {
        $.ajax({
            type: 'get',
            url: 'getdepartment/'+id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function (data) {
                $('#department_name1').val(data.name);
                $("#organization_name option[value='"+data.organization_id+"']").attr('selected','selected');
                $('#update_dep').attr('onclick','update_department('+data.id+')');
//              $('#update_dep').attr('onclick','updateorganization('+data.id+')');

                $('#myModal1').modal('show');
                $('.listings').DataTable();

            }
        });
    }

    function update_department(id) {
        $.ajax({
            type  : 'put',
            url : 'department/'+id,
            data  : {
                '_token'      :$('input[name=_token]').val(),
                'name':$('#department_name1').val(),
                '#organization_name':$('#organization_name').val()
            },
            success:function (data) {
                $('#cardrander').html('');
                $('#cardrander').append(data);
                toastr.success('Data has been updated successfully!');
                $('.listings').DataTable();
            }
        });
    }
</script>
@endsection