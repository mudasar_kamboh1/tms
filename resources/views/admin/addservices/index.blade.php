@extends('admin.layouts.app')

@section('content')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2><a href="{{ url()->previous() }}" class="btn btn-xs btn-link"><i
                                        class="fa fa-arrow-left"></i></a>Services</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item active">Services</li>
                        </ul>
                    </div>
                </div>
            </div>

            @can('Add Services')
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <h5> Add Service</h5>
                            <form action="{{url('service')}}" method="post">
                                @csrf
                                <div class="row clearfix">
                                    <div class="col-md-3 col-sm-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="title" placeholder="Add Service">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Create</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @endcan
            <div class="row clearfix" id="cardrander">
                <div class="col-lg-12">
                    @can('View Services')
                        <div class="card">
                        <div class="body">
                            <h5>List</h5>
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered m-b-0 c_list">
                                    <thead>
                                    <tr>
                                        <th>Service</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        @foreach( $services as $service)
                                        <td>{{$service->title}}</td>
                                        <td>
                                            @can('Edit Services')
                                              <form action="{{url('service'.$service->id)}}" method="get">
                                                <a href="#" class="btn btn-info" title="Edit"><i class="fa fa-edit"></i></a>
                                              </form>
                                            @endcan

                                            @can('Delete Services')
                                                <form action="{{route('service.destroy',$service->id)}}" method="POST">
                                                    <input type="hidden" value="DELETE" name="_method">
                                                    {{@csrf_field()}}
                                                    <input type="submit" value="delete" name="delete" class="btn btn-danger">
                                                </form>
                                            @endcan
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    @endcan
                </div>
            </div>
        </div>
    </div>


@endsection

