@extends('admin.layouts.app')

@section('content')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2><a href="{{ url()->previous() }}" class="btn btn-xs btn-link"><i
                                        class="fa fa-arrow-left"></i></a>Teams</h2>
                    </div>
                </div>
            </div>
            <div class="row clearfix" id="cardrander">
                <div class="col-lg-12">
                    <div class="card" style="min-height: 500px">
                        <div class="header">
                            <ul class="header-dropdown">
                                <li>
                                    @can('Add Team')
                                        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add Team</button>
                                    @endcan
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            @can('View Team')
                                <table class="table table-hover m-b-0 c_list">
                                    <thead>
                                    <tr>
                                        <th>Team Name</th>
                                        <th>Department Name</th>
                                        <th>Organization Name</th>
                                        <th>Created at</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($teams))
                                        @foreach($teams as $team)
                                            <tr>
                                                <td><a href="{{ url('getTeamMember/'.$team->id) }}">{{ $team->name }}</a></td>
                                                <td>{{ !is_null($team->department) ?  $team->department->name : '-- -- --' }}</td>
                                                <td>{{ !is_null($team->department) && !is_null($team->department->organization) ? $team->department->organization->name : '-- -- --'}}</td>
                                                <td>{{ !is_null($team->created_at) ? \Carbon\Carbon::parse($team->created_at)->format('h:i A d-m-Y') : '' }}</td>
                                                <td class="text-center">
                                                    @can('Delete Team')
                                                        <form action="{{ route('team.destroy', $team->id) }}" method="post">
                                                            @method('DELETE')
                                                            @csrf
                                                            <button  type="submit" class="btn btn-danger js-sweetalert" title="Delete"><i class="fa fa-trash-o"></i></button>
                                                        </form>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            @endcan
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal1 -->
    @can('Add Team')
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Team</h4>
                </div>
                <div class="modal-body">
                    <form class="form-control" action="team" method="post">
                        @csrf
                        <div class="form-group">
                            <label class="label">Name*</label>
                            <input type="text" name="name" placeholder="Name" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label class="label">Services</label>
                            <div class="row">
                                <div class="col-10">
                                    <select data-placeholder="Choose Services..." id="services" class="chosen-select" tabindex="2" name="services[]" multiple data-height="40px">
                                        <option value="" disabled>Select Services</option>
                                        @foreach($services as $service)
                                            <option value="{{ $service->id }}">{{ $service->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-2">
                                    <a href="{{ url('service') }}" class="btn btn-info pull-right" target="_blank" title="Add Services">Add New Services</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="label">Attach to any organization?</label>
                            <select id="organization_val" name="organization_id" class="form-control" >
                                <option value="" disabled>Select Organization</option>
                                @foreach(App\Organization::all() as $org)
                                    <option value="{{ $org->id }}">{{ $org->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="label">Department</label>
                            <select id="dep_value" name="department_id" class="form-control" ></select>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-success" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @endcan

    <!-- Modal1 -->
    <div id="myModal1" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Team Member</h4>
                </div>
                <div class="modal-body">
                    <form class="form-control" action="teamMember" method="post">
                        @csrf
                        <div class="form-group">
                            <select name="teams_id" class="form-control">
                                <option>Select Team</option>
                                @foreach(App\Team::all() as $team)
                                    <option value="{{ $team->id }}">{{ $team->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" name="name" placeholder="Name" class="form-control" />
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" placeholder="Email" class="form-control" />
                        </div>
                        <div class="form-group">
                            <input type="text" name="mobile" placeholder="Phone" class="form-control" />
                        </div>
                        <div class="form-group">
                            <input type="text" name="skype" placeholder="Skype" class="form-control" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-success" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endsection
@section('js')

    <script type="text/javascript">
        $(function () {
            $('#organization_val').change(function () {
                var id = $(this).val();
                $.ajax({
                    type: 'get',
                    url: 'getodep/' + id,
                    data: {
                        '_token': $('input[name=_token]').val(),
                    },
                    success: function (data) {
//                        data = JSON.parse(data);
                        $('#dep_value').empty();
                        $('#dep_value').append("<option> Select Department</option>");

                        $.each(data, function(i, item) {
                            $('#dep_value').append("<option value='"+ item.id +"'>"+ item.name+"</option>");
                        });
                    }
                });
            });

        });
    </script>

    @endsection
