<div class="col-lg-12">
    <div class="card">
        <div class="header">
            <h2>List</h2>
            <ul class="header-dropdown">
                <li>
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add Organization</button>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive" >
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-hover js-basic-example dataTable table-custom m-b-0 no-footer listings"
                                   id="DataTables_Table_0" role="grid"
                                   aria-describedby="DataTables_Table_0_info">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0"
                                        aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                        style="width: 38.5833px;" aria-sort="ascending"
                                        aria-label="Name: activate to sort column descending">#
                                    </th>
                                    <th class="sorting_asc" tabindex="0"
                                        aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                        style="width: 38.5833px;" aria-sort="ascending"
                                        aria-label="Name: activate to sort column descending">Name
                                    </th>
                                    <th class="sorting_asc" tabindex="0"
                                        aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                        style="width: 38.5833px;" aria-sort="ascending"
                                        aria-label="Name: activate to sort column descending">Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="rows">
                                @php
                                    $x=1;
                                @endphp
                                {{--@foreach( App\Organization::all() as $name )--}}

                                    <tr role="row" class="odd">
                                        <td>{{ $x++ }}</td>
                                        <td class="width45 sorting_1">
                                        </td>
                                        <td>
                                            <button onclick="del_organization({})" class="btn btn-danger">Delete</button>
                                            <button onclick="edit_organization({})" class="btn btn-primary"  data-toggle="modal" data-target="#myModal1">Edit</button>
                                        </td>
                                    </tr>
                                {{--@endforeach--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>