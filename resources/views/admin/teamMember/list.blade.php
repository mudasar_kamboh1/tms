@extends('admin.layouts.app')

@section('content')
<style>
    input{
        position: relative !important;
        right: auto !important;
    }
</style>
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-sm-6">
                        <h2><a href="{{ url()->previous() }}" class="btn btn-xs btn-link"><i
                                        class="fa fa-arrow-left"></i></a>Team Members</h2>
                    </div>
                    <div class="col-sm-6">
                        @can('Add Team Member')
                            <button type="button" class="btn btn-outline-info pull-right" data-toggle="modal" data-target="#myModal1" style="margin-right: 5px">Add Team Member</button>
                        @endcan
                    </div>
                </div>
            </div>
            <div class="row clearfix" id="cardrander">
                <div class="col-lg-12">
                    <div class="card" style="min-height: 500px">
                        @can('View Team Member')
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Skype</th>
                                    <th>Account Manager</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($agents as $k => $agent)
                                    <tr>
                                        <td> {{ ++$k }}</td>
                                        <td>{{ $agent->user->name }}</td>
                                        <td>{{ $agent->user->email }}</td>
                                        <td>{{ $agent->user->mobile }}</td>
                                        <td>{{ $agent->user->skype }}</td>
                                        <td>{{ isset($agent->user->roles[0])? $agent->user->roles[0]->name : 'N/A'}}</td>
                                        <td>
                                            @can('Edit Team Member')
                                                <a href="{{ route('users.edit', $agent->user->id) }}" class="btn btn-info" ><i class="fa fa-edit" aria-hidden="true"></i></a>
                                            @endcan
                                            @can('Delete Team Member')
                                                <button type="button" class="btn btn-danger" onclick="del_team({{ $agent->id }})" ><i class="fa fa-remove" aria-hidden="true"></i></button>
                                            @endcan
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                            @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal1 -->
    <div id="myModal1" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Team Member</h4>
                </div>
                <div class="modal-body">
                    <form class="form-control" action="{{ url('teamMember') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" name="team_id" value="{{ $teams->id }}">
                        </div>
                        <div class="form-group">
                            <input type="text" name="name" placeholder="Name" class="form-control" />
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" placeholder="Email" class="form-control" />
                        </div>
                        <div class="form-group">
                            <input type="text" name="mobile" placeholder="Phone" class="form-control" />
                        </div>
                        <div class="form-group">
                            <input type="text" name="skype" placeholder="Skype" class="form-control" />
                        </div>
                        <div class="form-group">
                            <input type="text" name="password" placeholder="Password" class="form-control" />
                        </div>
                        <div class='form-group'>
                            @foreach ($roles as $role)
                                {{ Form::radio('roles[]',  $role->id ) }}
                                {{ Form::label($role->name, ucfirst($role->name)) }}<br>

                            @endforeach
                        </div>
                        <br>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-success" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal1 -->
    <div id="getteam" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Team</h4>
                </div>
                <div class="modal-body">
                    <form class="form-control" id="team_form" method="post">
                        @csrf
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <input type="text" id="team_name" name="name" placeholder="Name" class="form-control" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-success" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">

        function del_team(id){
            var r = confirm("Are you sure? This might be effect on Tasks!");
            if (r == true) {
                $.ajax({
                    type:'post',
                    url: "{{ url('delteammember/') }}/"+id,
                    data:{
                        '_token':$('input[name=_token]').val()
                    },
                    success:function (data) {
                        toastr.success('Data has been deleted successfully!');
                        location.reload();
                    }
                });
            }
        }

    </script>
@endsection
