@extends('admin.layouts.app')

@section('content')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2><a href="{{ url()->previous() }}" class="btn btn-xs btn-link"><i
                                        class="fa fa-arrow-left"></i></a>Team Services</h2>
                    </div>
                    @can('Add Services')
                        <div class="col-lg-6 col-md-8 col-sm-12 ">
                            <a href="{{ url('service') }}" class="btn btn-info btn-lg pull-right" >Add More Services</a>
                        </div>
                    @endcan
                </div>
            </div>
            <div class="row clearfix">
                @can('Add Team Services')
                    <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <h5> Manage Team's Services</h5>
                            <form action="{{ route('storeTeamsServices') }}" method="post">
                                @csrf
                                <div class="row clearfix">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label class="label">Teams*</label>
                                            <select class="form-control show-tick" name="team" required>
                                                <option value="" disabled>Select Team</option>
                                                @foreach($teams as $team)
                                                    <option value="{{ $team->id }}">{{ $team->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label class="label">Service*</label>
                                            <select data-placeholder="Select Service..." id="services" class="chosen-select" tabindex="2" name="services[]" required multiple>
                                                <option value="" disabled>Select Services</option>
                                                @foreach($services as $service)
                                                    <option value="{{ $service->id }}">{{ $service->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <div class="form-group">
                                            <br>
                                            <button type="submit" class="btn btn-primary">Create</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @endcan
            </div>
            <div class="row clearfix" id="cardrander">
                <div class="col-lg-12">
                    @can('View Team Services')
                        <div class="card">

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered m-b-0 c_list">
                                    <thead>
                                    <tr>

                                        <th>Team</th>
                                        <th>Services</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($teamsServices))
                                        @foreach($teamsServices as $team)
                                            <tr>
                                                <td>{{ $team->name }}</td>
                                                <td>
                                                    {{ (count($team->services) > 0) ? str_replace(array('[',']','"'),'', $team->services->pluck('title')) : 'N/A' }}
                                                </td>
                                                <td>
                                                    @can('Delete Team Services')
                                                        <a href="{{ url('deleteTeamServices', $team->id) }}" class="btn btn-danger" title="View"><i class="fa fa-trash"></i></a>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                        @else
                                            <tr>
                                               <td colspan="3"> <h6 class="text-center text-warning">Record Not Found!</h6></td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    @endcan
                </div>
            </div>
        </div>
    </div>


@endsection

