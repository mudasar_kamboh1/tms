<h5>Recent Comments</h5>
<div class="card-height">
    @if(count($comments))
        @foreach($comments as $comment)
            <div class="{{ $comment->teamMember && (\Illuminate\Support\Facades\Auth::user()->id == $comment->teamMember->user->id) ? 'warning' : 'green'}} timeline-item text-left clearfix">
                <span class="date"> {{ \Carbon\Carbon::parse($comment->created_at)->format('h:i A d-M-y') }}</span>
                <h6> {{ $comment->description }}</h6>
                <span>Added By: <a href="javascript:void(0);" title="Team Member">
                                {{ !is_null($comment->teamMember) && !is_null($comment->teamMember->user) ? $comment->teamMember->user->name : 'N/A' }}
                            </a></span>
            </div>
        @endforeach
    @endif
</div>