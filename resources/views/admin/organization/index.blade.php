@extends('admin.layouts.app')

@section('content')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2><a href="{{ url()->previous() }}" class="btn btn-xs btn-link"><i
                                        class="fa fa-arrow-left"></i></a>Organization</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item active">Organization</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row clearfix" id="cardrander">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>List</h2>
                            <ul class="header-dropdown">
                                <li>
                                    @can('Add Organization')
                                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal"
                                            data-target="#myModal">Add Organization
                                    </button>
                                    @endcan
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            @can('view Organization')
                                <div id="accordion">
                                @php
                                    $x=1;
                                @endphp
                                @foreach( App\Organization::with('departments')->get() as $organization )

                                    <div class="card" style="margin-bottom: 5px;">
                                        <div class="card-header">
                                            <a class="card-link" data-toggle="collapse" href="#collapse{{ $x }}">
                                                {{ $organization->name }}
                                            </a>
                                            @can('Delete Organization')
                                            <button type="button" class="btn btn-outline-danger"
                                                    onclick="del_organization({{ $organization->id }})"
                                                    style="padding: 4px 7px 3px 5px;float: right;margn-bootom:5px;margin-left: 2px;margin-top:-25px;">
                                                <i class="fa fa-remove" aria-hidden="true"></i></button>
                                            @endcan
                                            @can('Edit Organization')
                                            <button type="button" class="btn btn-outline-info"
                                                    onclick="edit_organization({{ $organization->id }})"
                                                    style="padding: 4px 7px 3px 5px;float: right;margin-top:-25px;"><i
                                                        class="fa fa-edit" aria-hidden="true"></i></button>
                                            @endcan
                                        </div>
                                        <div id="collapse{{ $x++ }}" class="collapse" data-parent="#accordion">
                                            <div class="card-body">
                                                <div class="form-group" style="display: flex;">
                                                    <input type="text" id="dep_name" name="name"
                                                           placeholder="Department Name" class="form-control" required
                                                           style="width: 25%">
                                                    <button type="button" class="btn btn-outline-info"
                                                            onclick="add_department($(this).siblings('input').val(),{{ $organization->id }})"
                                                            style="margin-right: 5px"><i class="fa fa-plus"
                                                                                         aria-hidden="true"></i>
                                                    </button>
                                                </div>
                                                {{--<ul style="list-style: none">--}}
                                                {{--@foreach($organization->departments as $department)--}}
                                                {{--<li style="padding: 10px 0;background-color: white;font-size: 20px;margin-bottom:5px;margin-left: 10px;border: 1px solid lightskyblue;border-radius: 5px"><span style="margin-left: 12px;">{{ $department->name }}</span>--}}
                                                {{--<button type="button" class="btn btn-outline-danger" onclick="del_dep({{ $department->id }})" style="padding: 4px 7px 3px 5px;float: right;margin-left: 2px;margin-bootom:5px;margin-right: 15px"><i class="fa fa-remove" aria-hidden="true"></i></button>--}}
                                                {{--<button type="button" class="btn btn-outline-info" onclick="edit_dep({{ $department->id }})" style="padding: 4px 7px 3px 5px;float: right;"><i class="fa fa-edit" aria-hidden="true"></i></button>--}}

                                                {{--</li>--}}
                                                {{--@endforeach--}}
                                                {{--</ul>--}}
                                                <div class="table-responsive">
                                                    <table class="table table-bordered  m-b-0 c_list">
                                                        <thead>
                                                        <tr>

                                                            <th>Dpartment Name</th>

                                                            <th>Action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($organization->departments as $department)
                                                            <tr>

                                                                <td width="50%">{{ $department->name }}</td>

                                                                <td width="50%">
                                                                    <button type="button" class="btn btn-outline-info"
                                                                            onclick="edit_dep({{ $department->id }})"><i
                                                                                class="fa fa-edit"
                                                                                aria-hidden="true"></i></button>
                                                                    <button type="button" class="btn btn-outline-danger"
                                                                            onclick="del_dep({{ $department->id }})"><i
                                                                                class="fa fa-remove"
                                                                                aria-hidden="true"></i></button>
                                                                </td>

                                                            </tr>
                                                        @endforeach

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @endcan
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    @can('Add Organization')
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Organization</h4>
                </div>
                <div class="modal-body">
                    <form action="organization" method="post">
                        @csrf
                        <div class="form-group">
                            <input type="text" id="organization_name" name="name" placeholder="Name"
                                   class="form-control" required/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-success" value="Add"/>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endcan
    <!-- Modal -->

    @can('Edit Organization')
    <div id="myModal1" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update Organization</h4>
                </div>
                <div class="modal-body">
                    <form id="organizationform" method="post">
                        {{ method_field('PUT') }}
                        @csrf
                        <div class="form-group">
                            <input type="text" id="organization_name1" name="name" placeholder="Name"
                                   class="form-control"/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-success" value="Update"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endcan
    <!-- Modal -->
    <div id="myModal2" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Department</h4>
                </div>
                <div class="modal-body">
                    <form action="department" method="post">
                        @csrf
                        <div class="form-group">
                            <input type="text" id="department_name" name="name" placeholder="Name" class="form-control"
                                   required/>
                        </div>
                        <div class="form-group">
                            <select name="organization_id" class="custom-select">
                                <option selected>Select Organization</option>
                                @foreach(App\Organization::all() as $data)
                                    <option value="{{$data->id}}"> {{ $data->name }} </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" value="Add"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal1 -->
    <div id="myModal3" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update Department</h4>
                </div>
                <div class="modal-body">
                    <form id="dep_form" method="post">
                        {{ method_field('PUT') }}
                        @csrf
                        <div class="form-group">
                            <input type="text" id="department_name1" name="name" placeholder="Name"
                                   class="form-control"/>
                        </div>
                        {{--<div class="form-group">--}}
                        {{--<select id="organization_name" class="custom-select">--}}
                        {{--<option selected>Select Organization</option>--}}
                        {{--@foreach(App\Organization::all() as $data)--}}
                        {{--<option value="{{$data->id}}"> {{ $data->name }} </option>--}}
                        {{--@endforeach--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-success" value="Update"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal1 -->

    @can('view Team Member')
    <div id="myModal5" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Team Member</h4>
                </div>
                <div class="modal-body">
                    <form class="form-control" action="teamMember" method="post">
                        @csrf
                        <div class="form-group">
                            <select id="select" name="organization_id">
                                <option>Select</option>
                                @foreach(App\Organization::all() as $dep)
                                    <option value="{{ $dep->id }}">{{ $dep->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="department_id">
                                <option>Select</option>
                                @foreach(App\Department::all() as $dep)
                                    <option value="{{ $dep->id }}">{{ $dep->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" name="name" placeholder="Name" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" placeholder="Email" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <input type="text" name="mobile" placeholder="Phone" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <input type="text" name="skype" placeholder="Skype" class="form-control"/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-success" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @endcan
    <!-- Modal1 -->

    @can('Edit Team Member')
    <div id="user" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Team Member</h4>
                </div>
                <div class="modal-body">
                    <form class="form-control" id="userform" method="post">
                        @csrf
                        <input name="_method" type="hidden" value="PUT">
                        <div class="form-group">
                            <select id="select" id="user_organization_id" name="organization_id">
                                <option>Select</option>
                                @foreach(App\Organization::all() as $dep)
                                    <option value="{{ $dep->id }}">{{ $dep->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select id="user_department_id" name="department_id">
                                <option>Select</option>
                                @foreach(App\Department::all() as $dep)
                                    <option value="{{ $dep->id }}">{{ $dep->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" id="user_name" name="name" placeholder="Name" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <input type="text" id="user_email" name="email" placeholder="Email" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <input type="text" id="user_mobile" name="mobile" placeholder="Phone" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <input type="text" id="user_skype" name="skype" placeholder="Skype" class="form-control"/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-success" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endcan

@endsection

@section('js')

    <script type="text/javascript">

        function edit_user(id) {
            $.ajax({
                type: 'get',
                url: 'teamMember/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function (data) {
                    $('#userform').attr('action', '/teamMembers/' + data.id);
                    $('#user_department_id').val(data.department_id);
                    $('#user_name').val(data.name);
                    $('#user_email').val(data.email);
                    $('#user_skype').val(data.skype);
                    $('#user_mobile').val(data.mobile);
                    $('#user').modal('show');
                }
            });
        }

        //---------Organization ACrud JS
        //        function add_organization(){
        //                alert('asd');
        //            $.ajax({
        //                type : 'post',
        //                route: 'organization',
        //                data:{
        //                    '_token':$('input[name=_token]').val(),
        //                    'name':$('input[name=organization_name]').val()
        //                },
        //                success:function (data) {
        //                    toastr.success('Data has been added successfully!');
        //                    $('#cardrander').html('');
        //                    $('#cardrander').append(data);
        //                    $('.listings').DataTable();
        //
        //                }
        //            });
        //        }

        //
        function del_organization(id) {
            var r = confirm("Press a button!");
            if (r == true) {
                $.ajax({
                    type: 'post',
                    url: "{{ url('delorganization/') }}/" + id,
                    data: {
                        '_token': $('input[name=_token]').val()
                    },
                    success: function (data) {
                        toastr.success('Data has been deleted successfully!');
                        location.reload();
                    }
                });
            }
        }

        //
        function edit_organization(id) {
            $.ajax({
                type: 'get',
                url: 'getorganization/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function (data) {
                    $('#organizationform').attr('action', '/organization/' + data.id);
                    $('#organization_name1').val(data.name);
                    $('#update').attr('onclick', 'updateorganization(' + data.id + ')');
                    $('#myModal1').modal('show');
                    $('.listings').DataTable();

                }
            });
        }

        //---End JS Organization
        function updateorganization(id) {

            $.ajax({
                type: 'put',
                url: 'organization/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                    'name': $('#organization_name1').val()
                },
                success: function () {
                    window.open('self.location', '_self');
                    //                    $('#cardrander').html('');
//
//                    $('#cardrander').append(data);
//                    toastr.success('Data has been updated successfully!');
//                    $('.listings').DataTable();

                }
            });
        }

        ////////////////////////////////////////////////////////////////
        function add_department(name, id) {
            $.ajax({
                type: 'post',
                url: "{{ route('department') }}",
                data: {
                    '_token': $('input[name=_token]').val(),
                    'name': name,
                    'organization_id': id
                },
                success: function (data) {
                    if (data.status == false) {
                        toastr.error('Name Already Exist!');
                    } else {
                        toastr.success('Data has been Added successfully!');
                        location.reload();
                    }
                }
            });
        }

        //---------del
        function del_dep(id) {
            var r = confirm("Press a button!");
            if (r == true) {
                $.ajax({
                    type: 'post',
                    url: "{{ url('department/') }}/" + id,
                    data: {
                        '_token': $('input[name=_token]').val()
                    },
                    success: function (data) {
                        location.reload();
                        toastr.success('Data has been deleted successfully!');

                    }
                });
            }
        }

        //-----EDIT------
        function edit_dep(id) {
            $.ajax({
                type: 'get',
                url: 'getdepartment/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function (data) {
                    $('#dep_form').attr('action', '/updatedepartment/' + data.id);
                    $('#department_name1').val(data.name);
                    $('#myModal3').modal('show');
                    $('.listings').DataTable();

                }
            });
        }

        function update_department(id) {
            $.ajax({
                type: 'put',
                url: 'department/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                    'name': $('#department_name1').val(),
                    '#organization_name': $('#organization_name').val()
                },
                success: function (data) {
                    $('#cardrander').html('');
                    $('#cardrander').append(data);
                    toastr.success('Data has been updated successfully!');
                    $('.listings').DataTable();
                    location.reload();
                }
            });
        }
    </script>

@endsection