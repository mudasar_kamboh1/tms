@extends('admin.layouts.app')

@section('title', '| Edit User')

@section('content')
    <style>
        input{
            position: relative !important;
            right: auto !important;
        }
    </style>
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2><a href="{{ url()->previous() }}" class="btn btn-xs btn-link"><i
                                        class="fa fa-arrow-left"></i></a>Edit {{$user->name}}</h2>
                        <ul class="breadcrumb">

                        </ul>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="body">

                            <div class='col-lg-4 col-lg-offset-4'>

                                {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with user data --}}

                                <div class="form-group">
                                    {{ Form::label('name', 'Name') }}
                                    {{ Form::text('name', null, array('class' => 'form-control')) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('email', 'Email') }}
                                    {{ Form::email('email', null, array('class' => 'form-control')) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('team', 'Team') }}
                                    <select data-placeholder="Select Type..." class="chosen-select" tabindex="2" name="team" required>
                                        <option value="">Select Team</option>
                                        @foreach($teams as $team)
                                            <option value="{{ $team->id }}" {{ $team->id == $user->teamMember->team->id ? 'selected' : ''}}>{{ $team->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <h5><b>Give Role</b></h5>

                                <div class='form-group'>
                                    @foreach ($roles as $role)
                                        <input type="radio" name="roles[]" value="{{ $role->id }}" {{ count($user->roles) > 0 && $user->roles[0]->id == $role->id ? 'checked' : ''  }}>
                                        {{ Form::label($role->name, ucfirst($role->name)) }}<br>
                                    @endforeach
                                </div>

                                <div class="form-group">
                                    {{ Form::label('password', 'Password') }}<br>
                                    {{ Form::password('password', array('class' => 'form-control')) }}

                                </div>

                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('password', 'Confirm Password') }}<br>--}}
                                    {{--{{ Form::password('password_confirmation', array('class' => 'form-control')) }}--}}

                                {{--</div>--}}

                                {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

                                {{ Form::close() }}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection