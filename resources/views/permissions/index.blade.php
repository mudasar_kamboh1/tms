{{-- \resources\views\permissions\index.blade.php --}}
@extends('admin.layouts.app')

@section('title', '| Add User')

@section('content')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2><a href="{{ url()->previous() }}" class="btn btn-xs btn-link"><i
                                        class="fa fa-arrow-left"></i></a> Available Permissions</h2>
                        <ul class="breadcrumb">

                        </ul>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="body">
                            <div class="col-lg-12">

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">

                                    <thead>
                                    <tr>
                                        <th>Permissions</th>
                                        <th>Operation</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($permissions as $permission)
                                        <tr>
                                            <td>{{ $permission->name }}</td>
                                            <td>
                                                <a href="{{ URL::to('permissions/'.$permission->id.'/edit') }}" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>

                                                {!! Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id] ]) !!}
                                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                                {!! Form::close() !!}

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <a href="{{ URL::to('permissions/create') }}" class="btn btn-success">Add Permission</a>
                                <div class="pull-right">
                                    {{ $permissions->render() }}
                                </div>
                        </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection