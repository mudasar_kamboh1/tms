{{-- \resources\views\permissions\index.blade.php --}}
@extends('admin.layouts.app')

@section('title', '| Add User')

@section('content')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2><a href="{{ url()->previous() }}" class="btn btn-xs btn-link"><i
                                        class="fa fa-arrow-left"></i></a> Edit {{$permission->name}}</h2>
                        <ul class="breadcrumb">

                        </ul>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="body">
                            <div class='col-lg-4 col-lg-offset-4'>
                                {{ Form::model($permission, array('route' => array('permissions.update', $permission->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with permission data --}}

                                <div class="form-group">
                                    {{ Form::label('name', 'Permission Name') }}
                                    {{ Form::text('name', null, array('class' => 'form-control')) }}
                                </div>
                                <br>
                                {{ Form::submit('Edit', array('class' => 'btn btn-primary')) }}

                                {{ Form::close() }}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection